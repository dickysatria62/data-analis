select * from `p9-proceduresdetails`;
select * from `p9-owners`;
select * from `p9-pets`;
select * from `p9-procedureshistory`;


#1
select `p9-pets`.`Name` as Pet_Name,`p9-owners`.`Name` as Owner_Name from `p9-pets` inner join
    `p9-owners` on `p9-pets`.OwnerID = `p9-owners`.OwnerID;

#2
select `p9-pets`.`Name`, `p9-procedureshistory`.ProcedureType from `p9-pets` inner join
    `p9-procedureshistory` on `p9-procedureshistory`.PetID = `p9-pets`.PetID;

#3
select `PetID`, `Date`, `p9-procedureshistory`.ProcedureType , `p9-proceduresdetails`.`Description`
from `p9-procedureshistory`
    inner join `p9-proceduresdetails`
        on `p9-proceduresdetails`.`ProcedureSubCode` = `p9-procedureshistory`.`ProcedureSubCode`;

#4
select `Name`, `p9-procedureshistory`.`ProcedureType`, `p9-proceduresdetails`.`Description` from `p9-pets`
    inner join `p9-procedureshistory` on `p9-procedureshistory`.PetID = `p9-pets`.PetID
    inner join `p9-proceduresdetails` on `p9-proceduresdetails`.ProcedureSubCode = `p9-procedureshistory`.ProcedureSubCode;

#5
select `p9-owners`.Name as Owner,`p9-pets`.`Name` as Pet, `p9-procedureshistory`.ProcedureType, `p9-proceduresdetails`.Description, `p9-proceduresdetails`.Price
from `p9-owners` inner join
    `p9-pets` on `p9-pets`.OwnerID = `p9-owners`.OwnerID inner join
    `p9-procedureshistory` on `p9-procedureshistory`.PetID = `p9-pets`.PetID inner join
    `p9-proceduresdetails` on `p9-proceduresdetails`.ProcedureSubCode = `p9-procedureshistory`.ProcedureSubCode;


