select * from `p9-consolegames`;


select * from `p9-consoledates`;


ALTER TABLE `p9-consolegames` ADD COLUMN global_sales float8;

UPDATE `p9-consolegames` SET global_sales = NA_Sales + EU_Sales + JP_Sales + Other_Sales;

# Section 5
#1
SELECT sum(`NA_Sales`) as `Total NA_Sales`,sum(`global_sales`) as `Total Global Sales`,
sum(`NA_Sales`) * 100 / sum(`global_sales`) as 'Percentage of Total'
From `p9-consolegames`;

#2
select * from `p9-consolegames`
 order by `Platform` ASC, `Year` DESC;

#3
select  `Name`, LEFT(`Publisher`, 4) as First_Four_Letters from `p9-consolegames`;

#4
SELECT Platform from `p9-consoledates` where MONTH(Discontinued) < 12;

#5
select  Platform , YEAR(curdate()) - YEAR(Discontinued) as tanggal from `p9-consoledates`
order by Discontinued desc ;

#6
# Misalnya data type game_year yang tadinya integer maka akan diubah menjadi string maka
# contoh querynya adalah sebagai berikut
ALTER TABLE `p9-consolegames`
MODIFY `Year` bigint;


#7
# Sejauh ini pendekatan yang paling umum untuk data yang hilang adalah dengan menghilangkan kasus-kasus
# dengan data yang hilang dan menganalisis data yang tersisa .
# Pendekatan ini dikenal sebagai analisis kasus lengkap (atau kasus yang tersedia) atau penghapusan daftar.





#CHART

select count(NA_Sales) from `p9-consolegames`;
select count(JP_Sales) from `p9-consolegames`;
select count(EU_Sales) from `p9-consolegames`;
select count(Other_Sales) from `p9-consolegames`;
select * from `p9-consolegames`;
select * from `p9-consoledates`;

select
  `Year` as Tahun,
  COUNT(`NA_Sales`) as Penjualan_NA
from `p9-consolegames`
group by `Year`
order by `Year`;


SELECT sum(`NA_Sales`) as `Total NA_Sales`,sum(`global_sales`) as `Total Global Sales`,
sum(`NA_Sales`) * 100 / sum(`global_sales`) as 'Percentage of Total'
From `p9-consolegames`;

SELECT sum(`EU_Sales`) as `Total EU_Sales`,sum(`global_sales`) as `Total Global Sales`,
sum(`EU_Sales`) * 100 / sum(`global_sales`) as 'Percentage of Total'
From `p9-consolegames`;

SELECT sum(`JP_Sales`) as `Total JP_Sales`,sum(`global_sales`) as `Total Global Sales`,
sum(`JP_Sales`) * 100 / sum(`global_sales`) as 'Percentage of Total'
From `p9-consolegames`;

SELECT sum(`Other_Sales`) as `Total JP_Sales`,sum(`global_sales`) as `Total Global Sales`,
sum(`Other_Sales`) * 100 / sum(`global_sales`) as 'Percentage of Total'
From `p9-consolegames`;

